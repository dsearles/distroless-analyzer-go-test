package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	report := `{
  "version": "2.4",
  "vulnerabilities": [
    {
      "category": "sast",
      "name": "Distroless test!",
      "message": "distroless test: Errors unhandled.",
      "description": "This is a distroless test.",
      "cve": "main.go:22:t.Execute(os.Stdout, v):CWE-703",
      "severity": "Low",
      "confidence": "High",
      "scanner": {
        "id": "distroless-go",
        "name": "distroless-go"
      },
      "location": {
        "file": "main.go",
        "start_line": 22
      },
      "identifiers": [
        {
          "type": "distroless_rule_id",
          "name": "Distroless Rule ID G104",
          "value": "G104",
          "url": "https://securego.io/docs/rules/g104.html"
        },
        {
          "type": "CWE",
          "name": "CWE-703",
          "value": "703",
          "url": "https://cwe.mitre.org/data/definitions/703.html"
        }
      ]
    }
  ],
  "remediations": []
}
`

	reportFile := "gl-sast-report.json"
	data := []byte(report)
	err := ioutil.WriteFile(reportFile, data, 0644)
	if err != nil {
		panic(err)
	}

	contents, err := ioutil.ReadFile(reportFile)
	if err != nil {
		panic(err)
	}

	contentsString := string(contents[:])
	fmt.Println(contentsString)
}
